const c = @import("c.zig");

const r2d = @import("render2d.zig");
const Viewport = r2d.Viewport;
const rectc = r2d.rectc;

const assets = @import("assets.zig");
const Texture = assets.Texture;
const Image = assets.Image;

usingnamespace @import("math3d.zig");
usingnamespace @import("core.zig");
// ======================================= //

var win_width: f32 = 800;
var win_height: f32 = 600;

var view = Viewport{
    .width = 800,
    .height = 600,
    .x_offset = 0,
    .y_offset = 0,
    .pixel_per_unity = 64.0,
};

var hud = Viewport{
    .width = 800,
    .height = 600,
    .x_offset = 0,
    .y_offset = 0,
    .pixel_per_unity = 128.0,
};

// ===== Lua functions =====
inline fn luaDoFile(L: *c.lua_State, file_name: [*c]const u8) c_int {
    var result = c.luaL_loadfilex(L, file_name, null);
    if (result == 0) {
        result = c.lua_pcallk(L, 0, c.LUA_MULTRET, 0, 0, null);
    }
    return result;
}

inline fn luaPop(L: *c.lua_State, n: c_int) void {
    c.lua_settop(L, (-n) - 1);
}

inline fn luaCall(L: *c.lua_State, n: c_int, r: c_int) void {
    c.lua_callk(lua_state, n, r, 0, null);
}

inline fn luaRegister(
    L: *c.lua_State,
    name: [*c]const u8,
    function: @TypeOf(render),
) void {
    c.lua_pushcclosure(L, function, 0);
    c.lua_setglobal(L, name);
}
// =========================

var font_texture: Texture = undefined;
var potato: Texture = undefined;

var tex_table: [9]Texture = undefined;

var lua_state: *c.lua_State = undefined;

fn drawText(LN: ?*c.lua_State) callconv(.C) c_int {
    var L: *c.lua_State = LN orelse unreachable;

    const idx = c.lua_gettop(L);
    var pos_x: f32 = 0;
    var pos_y: f32 = 0;
    var string: [*c]const u8 = undefined;

    //const idx = c.lua_gettop(L);
    pos_x = @floatCast(f32, c.lua_tonumberx(L, 1, null));
    pos_y = @floatCast(f32, c.lua_tonumberx(L, 2, null));
    string = c.lua_tolstring(L, 3, null);

    var str_len: usize = 0;

    var i: usize = 0;
    while (string[i] != 0) : (i += 1) {}
    str_len = i;

    r2d.drawBitmapString(
        string[0..str_len],
        @intCast(u8, i),
        font_texture,
        rectc(
            pos_x,
            pos_y,
            0.2,
            0.24,
        ),
        10,
        12,
        -0.1,
        hud,
    );
    return 0;
}

fn render(LN: ?*c.lua_State) callconv(.C) c_int {
    var L: *c.lua_State = LN orelse unreachable;

    var size_x: f32 = 0;
    var size_y: f32 = 0;

    var pos_x: f32 = 0.0;
    var pos_y: f32 = 0.0;
    var rot: f32 = 0.0;

    const idx = c.lua_gettop(L);
    const idx_p1 = idx + 1;
    const idx_p2 = idx + 2;

    _ = c.lua_getfield(L, idx, "mySize");
    _ = c.lua_getfield(L, idx_p1, "x");
    size_x = @floatCast(f32, c.lua_tonumberx(L, idx_p2, null));
    luaPop(L, 1);
    _ = c.lua_getfield(L, idx_p1, "y");
    size_y = @floatCast(f32, c.lua_tonumberx(L, idx_p2, null));
    luaPop(L, 1);
    luaPop(L, 1);

    _ = c.lua_getfield(L, idx, "myPosition");
    _ = c.lua_getfield(L, idx_p1, "x");
    pos_x = @floatCast(f32, c.lua_tonumberx(L, idx_p2, null));
    luaPop(L, 1);
    _ = c.lua_getfield(L, idx_p1, "y");
    pos_y = @floatCast(f32, c.lua_tonumberx(L, idx_p2, null));
    luaPop(L, 1);
    luaPop(L, 1);

    _ = c.lua_getfield(L, idx, "myRotation");
    _ = c.lua_getfield(L, idx_p1, "v");
    rot = @floatCast(f32, c.lua_tonumberx(L, idx_p2, null));
    luaPop(L, 1);
    luaPop(L, 1);

    _ = c.lua_getfield(L, idx, "mySprite");
    var tex_index = @floatToInt(i32, c.lua_tonumberx(L, idx_p1, null));

    if (tex_index < 0) tex_index = 0;
    if (tex_index >= tex_table.len) tex_index = tex_table.len - 1;

    luaPop(L, 1);
    const tex: Texture = tex_table[@intCast(usize, tex_index)];

    r2d.drawTexture(
        tex,
        rectc(pos_x, pos_y, size_x, size_y),
        rectc(0, 0, 1, 1),
        view,
        0,
        rot,
    );

    return 0;
}

/// Execute after initialization and before the main loop
fn start(ctx: Ctx) void {
    potato = assets.potato_img.toGlTexture();
    tex_table[0] = potato;
    font_texture = assets.font_img.toGlTexture();

    tex_table[1] = assets.embedBmpToGlTexture("slime");
    tex_table[2] = assets.embedBmpToGlTexture("spiky_slime");

    tex_table[3] = assets.embedBmpToGlTexture("healer");
    tex_table[4] = assets.embedBmpToGlTexture("healer_hurt");

    tex_table[5] = assets.embedBmpToGlTexture("knight");
    tex_table[6] = assets.embedBmpToGlTexture("knight_hurt");

    tex_table[7] = assets.embedBmpToGlTexture("gost_tree");
    tex_table[8] = assets.embedBmpToGlTexture("geon");

    luaRegister(lua_state, "Render", render);
    luaRegister(lua_state, "DrawText", drawText);
    _ = luaDoFile(lua_state, "definitions.lua");
    _ = luaDoFile(lua_state, "project.lua");
    _ = c.lua_getglobal(lua_state, "media");

    const idx = c.lua_gettop(lua_state);
    const idx_p1 = idx + 1;
    const idx_p2 = idx + 2;

    const len: usize = c.lua_rawlen(lua_state, idx);

    var i: usize = 1;
    while (i <= len) : (i += 1) {
        _ = c.lua_rawgeti(lua_state, idx, @intCast(c_longlong, i));
        _ = c.lua_getfield(lua_state, idx_p1, "media_type");
        const lua_type = c.lua_tonumberx(lua_state, idx_p2, null);
        luaPop(lua_state, 1);

        if (lua_type == 0) {
            _ = c.lua_getfield(lua_state, idx_p1, "path");
            const path = c.lua_tolstring(lua_state, idx_p2, null);
            luaPop(lua_state, 1);

            _ = c.lua_getfield(lua_state, idx_p1, "texture_id");
            const tex_id = @floatToInt(u32, c.lua_tonumberx(lua_state, idx_p2, null));
            luaPop(lua_state, 1);
        } else {
            //
        }
        luaPop(lua_state, 1);
    }

    _ = c.lua_getglobal(lua_state, "init");
    _ = c.lua_callk(lua_state, 0, 0, 0, null);
    luaPop(lua_state, 1);
}

/// Upadates every frame, it is called from the init function
fn update_and_renderer(ctx: Ctx, delta: f64) void {
    hud.width = @intToFloat(f32, ctx.window_width);
    hud.height = @intToFloat(f32, ctx.window_height);

    _ = c.lua_getglobal(lua_state, "UpdateActors");

    c.lua_pushnumber(lua_state, delta);
    c.lua_pushinteger(lua_state, input.input_down);
    c.lua_pushinteger(lua_state, input.input_up);
    c.lua_pushinteger(lua_state, input.input_pressed);

    luaCall(lua_state, 4, 1);
    luaPop(lua_state, 1);

    drawDebugHud(ctx, delta);
}

fn drawDebugHud(ctx: Ctx, delta: f64) void {
    view.width = @intToFloat(f32, ctx.window_width);
    view.height = @intToFloat(f32, ctx.window_height);

    const fps = @floatToInt(i32, ((1 / delta) + 0.5));

    r2d.drawBitmapStringFmtPx(
        "{}fps\n{d:0.2}ms",
        .{
            fps,
            delta * 1000,
        },
        128,
        font_texture,
        rectc(
            8 - hud.width / 2.0,
            -16 + hud.height / 2.0,
            10 * 2,
            12 * 2,
        ),
        10,
        12,
        -0.1,
        hud,
    );
}

pub fn main() anyerror!void {
    if (c.luaL_newstate()) |state| {
        lua_state = state;
    } else {
        @panic("Unable to create lua state");
    }
    c.luaL_openlibs(lua_state);
    _ = luaDoFile(lua_state, "configs.lua");

    _ = c.lua_getglobal(lua_state, "game");

    const idx: i32 = c.lua_gettop(lua_state);

    //read resolution
    _ = c.lua_getfield(lua_state, idx, "resolution");

    _ = c.lua_rawgeti(lua_state, idx + 1, 1);
    win_width = @floatCast(f32, c.lua_tonumberx(lua_state, idx + 2, null));
    luaPop(lua_state, 1);

    _ = c.lua_rawgeti(lua_state, idx + 1, 2);
    win_height = @floatCast(f32, c.lua_tonumberx(lua_state, idx + 2, null));
    luaPop(lua_state, 1);

    luaPop(lua_state, 1);
    luaPop(lua_state, 1);

    const ctx = try init(
        @floatToInt(u32, win_width),
        @floatToInt(u32, win_height),
        "jaminator",
        start,
        update_and_renderer,
    );
    quit(ctx);

    c.lua_close(lua_state);
}
