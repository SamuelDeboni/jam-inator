pub const Mat4 = struct {
    data: [4][4]f32,

    pub fn mult(m: Mat4, other: Mat4) Mat4 {
        return Mat4{
            .data = [_][4]f32{
                [_]f32{
                    m.data[0][0] * other.data[0][0] + m.data[0][1] * other.data[1][0] + m.data[0][2] * other.data[2][0] + m.data[0][3] * other.data[3][0],
                    m.data[0][0] * other.data[0][1] + m.data[0][1] * other.data[1][1] + m.data[0][2] * other.data[2][1] + m.data[0][3] * other.data[3][1],
                    m.data[0][0] * other.data[0][2] + m.data[0][1] * other.data[1][2] + m.data[0][2] * other.data[2][2] + m.data[0][3] * other.data[3][2],
                    m.data[0][0] * other.data[0][3] + m.data[0][1] * other.data[1][3] + m.data[0][2] * other.data[2][3] + m.data[0][3] * other.data[3][3],
                },
                [_]f32{
                    m.data[1][0] * other.data[0][0] + m.data[1][1] * other.data[1][0] + m.data[1][2] * other.data[2][0] + m.data[1][3] * other.data[3][0],
                    m.data[1][0] * other.data[0][1] + m.data[1][1] * other.data[1][1] + m.data[1][2] * other.data[2][1] + m.data[1][3] * other.data[3][1],
                    m.data[1][0] * other.data[0][2] + m.data[1][1] * other.data[1][2] + m.data[1][2] * other.data[2][2] + m.data[1][3] * other.data[3][2],
                    m.data[1][0] * other.data[0][3] + m.data[1][1] * other.data[1][3] + m.data[1][2] * other.data[2][3] + m.data[1][3] * other.data[3][3],
                },
                [_]f32{
                    m.data[2][0] * other.data[0][0] + m.data[2][1] * other.data[1][0] + m.data[2][2] * other.data[2][0] + m.data[2][3] * other.data[3][0],
                    m.data[2][0] * other.data[0][1] + m.data[2][1] * other.data[1][1] + m.data[2][2] * other.data[2][1] + m.data[2][3] * other.data[3][1],
                    m.data[2][0] * other.data[0][2] + m.data[2][1] * other.data[1][2] + m.data[2][2] * other.data[2][2] + m.data[2][3] * other.data[3][2],
                    m.data[2][0] * other.data[0][3] + m.data[2][1] * other.data[1][3] + m.data[2][2] * other.data[2][3] + m.data[2][3] * other.data[3][3],
                },
                [_]f32{
                    m.data[3][0] * other.data[0][0] + m.data[3][1] * other.data[1][0] + m.data[3][2] * other.data[2][0] + m.data[3][3] * other.data[3][0],
                    m.data[3][0] * other.data[0][1] + m.data[3][1] * other.data[1][1] + m.data[3][2] * other.data[2][1] + m.data[3][3] * other.data[3][1],
                    m.data[3][0] * other.data[0][2] + m.data[3][1] * other.data[1][2] + m.data[3][2] * other.data[2][2] + m.data[3][3] * other.data[3][2],
                    m.data[3][0] * other.data[0][3] + m.data[3][1] * other.data[1][3] + m.data[3][2] * other.data[2][3] + m.data[3][3] * other.data[3][3],
                },
            },
        };
    }

    /// Builds a rotation 4 * 4 matrix created from an axis vector and an angle.
    /// Input matrix multiplied by this rotation matrix.
    /// angle: Rotation angle expressed in radians.
    /// axis: Rotation axis, has to be normalized.
    pub fn rotate(m: Mat4, angle: f32, axis: Vec3) Mat4 {
        const cos = @cos(angle);
        const s = @sin(angle);
        const temp = axis.scale(1.0 - cos);

        const rot = Mat4{
            .data = [_][4]f32{
                [_]f32{ cos + temp.x * axis.x, 0.0 + temp.y * axis.x - s * axis.z, 0.0 + temp.z * axis.x + s * axis.y, 0.0 },
                [_]f32{ 0.0 + temp.x * axis.y + s * axis.z, cos + temp.y * axis.y, 0.0 + temp.z * axis.y - s * axis.x, 0.0 },
                [_]f32{ 0.0 + temp.x * axis.z - s * axis.y, 0.0 + temp.y * axis.z + s * axis.x, cos + temp.z * axis.z, 0.0 },
                [_]f32{ 0.0, 0.0, 0.0, 0.0 },
            },
        };

        return Mat4{
            .data = [_][4]f32{
                [_]f32{
                    m.data[0][0] * rot.data[0][0] + m.data[0][1] * rot.data[1][0] + m.data[0][2] * rot.data[2][0],
                    m.data[0][0] * rot.data[0][1] + m.data[0][1] * rot.data[1][1] + m.data[0][2] * rot.data[2][1],
                    m.data[0][0] * rot.data[0][2] + m.data[0][1] * rot.data[1][2] + m.data[0][2] * rot.data[2][2],
                    m.data[0][3],
                },
                [_]f32{
                    m.data[1][0] * rot.data[0][0] + m.data[1][1] * rot.data[1][0] + m.data[1][2] * rot.data[2][0],
                    m.data[1][0] * rot.data[0][1] + m.data[1][1] * rot.data[1][1] + m.data[1][2] * rot.data[2][1],
                    m.data[1][0] * rot.data[0][2] + m.data[1][1] * rot.data[1][2] + m.data[1][2] * rot.data[2][2],
                    m.data[1][3],
                },
                [_]f32{
                    m.data[2][0] * rot.data[0][0] + m.data[2][1] * rot.data[1][0] + m.data[2][2] * rot.data[2][0],
                    m.data[2][0] * rot.data[0][1] + m.data[2][1] * rot.data[1][1] + m.data[2][2] * rot.data[2][1],
                    m.data[2][0] * rot.data[0][2] + m.data[2][1] * rot.data[1][2] + m.data[2][2] * rot.data[2][2],
                    m.data[2][3],
                },
                [_]f32{
                    m.data[3][0] * rot.data[0][0] + m.data[3][1] * rot.data[1][0] + m.data[3][2] * rot.data[2][0],
                    m.data[3][0] * rot.data[0][1] + m.data[3][1] * rot.data[1][1] + m.data[3][2] * rot.data[2][1],
                    m.data[3][0] * rot.data[0][2] + m.data[3][1] * rot.data[1][2] + m.data[3][2] * rot.data[2][2],
                    m.data[3][3],
                },
            },
        };
    }

    /// Builds a translation 4 * 4 matrix created from a vector of 3 components.
    /// Input matrix multiplied by this translation matrix.
    pub fn translate(m: Mat4, x: f32, y: f32, z: f32) Mat4 {
        return Mat4{
            .data = [_][4]f32{
                [_]f32{ m.data[0][0], m.data[0][1], m.data[0][2], m.data[0][3] + m.data[0][0] * x + m.data[0][1] * y + m.data[0][2] * z },
                [_]f32{ m.data[1][0], m.data[1][1], m.data[1][2], m.data[1][3] + m.data[1][0] * x + m.data[1][1] * y + m.data[1][2] * z },
                [_]f32{ m.data[2][0], m.data[2][1], m.data[2][2], m.data[2][3] + m.data[2][0] * x + m.data[2][1] * y + m.data[2][2] * z },
                [_]f32{ m.data[3][0], m.data[3][1], m.data[3][2], m.data[3][3] },
            },
        };
    }

    pub fn translateByVec(m: Mat4, v: Vec3) Mat4 {
        return m.translate(v.data[0], v.data[1], v.data[2]);
    }

    /// Builds a scale 4 * 4 matrix created from 3 scalars.
    /// Input matrix multiplied by this scale matrix.
    pub fn scale(m: Mat4, x: f32, y: f32, z: f32) Mat4 {
        return Mat4{
            .data = [_][4]f32{
                [_]f32{ m.data[0][0] * x, m.data[0][1] * y, m.data[0][2] * z, m.data[0][3] },
                [_]f32{ m.data[1][0] * x, m.data[1][1] * y, m.data[1][2] * z, m.data[1][3] },
                [_]f32{ m.data[2][0] * x, m.data[2][1] * y, m.data[2][2] * z, m.data[2][3] },
                [_]f32{ m.data[3][0] * x, m.data[3][1] * y, m.data[3][2] * z, m.data[3][3] },
            },
        };
    }

    pub fn transpose(m: Mat4) Mat4 {
        return Mat4{
            .data = [_][4]f32{
                [_]f32{ m.data[0][0], m.data[1][0], m.data[2][0], m.data[3][0] },
                [_]f32{ m.data[0][1], m.data[1][1], m.data[2][1], m.data[3][1] },
                [_]f32{ m.data[0][2], m.data[1][2], m.data[2][2], m.data[3][2] },
                [_]f32{ m.data[0][3], m.data[1][3], m.data[2][3], m.data[3][3] },
            },
        };
    }
};

pub const Mat4_identity = Mat4{
    .data = [_][4]f32{
        [_]f32{ 1.0, 0.0, 0.0, 0.0 },
        [_]f32{ 0.0, 1.0, 0.0, 0.0 },
        [_]f32{ 0.0, 0.0, 1.0, 0.0 },
        [_]f32{ 0.0, 0.0, 0.0, 1.0 },
    },
};

/// Creates a matrix for an orthographic parallel viewing volume.
pub fn Mat4Ortho(left: f32, right: f32, bottom: f32, top: f32) Mat4 {
    var m = Mat4_identity;
    m.data[0][0] = 2.0 / (right - left);
    m.data[1][1] = 2.0 / (top - bottom);
    m.data[2][2] = -1.0;
    m.data[0][3] = -(right + left) / (right - left);
    m.data[1][3] = -(top + bottom) / (top - bottom);
    return m;
}

pub const Vec3 = struct {
    x: f32,
    y: f32,
    z: f32,

    pub inline fn normalize(v: Vec3) Vec3 {
        return v.scale(1.0 / @sqrt(v.dot(v)));
    }

    pub inline fn scale(v: Vec3, scalar: f32) Vec3 {
        return Vec3{
            .x = v.x * scalar,
            .y = v.y * scalar,
            .z = v.z * scalar,
        };
    }

    pub inline fn dot(v: Vec3, other: Vec3) f32 {
        return v.x * other.x + v.y * other.y + v.z * other.z;
    }

    pub inline fn length(v: Vec3) f32 {
        return @sqrt(v.dot(v));
    }

    /// returns the cross product
    pub inline fn cross(v: Vec3, other: Vec3) Vec3 {
        return Vec3{
            .x = v.y * other.z - other.y * v.z,
            .y = v.z * other.x - other.z * v.x,
            .z = v.x * other.y - other.x * v.y,
        };
    }

    pub inline fn add(v: Vec3, other: Vec3) Vec3 {
        return Vec3{
            .x = v.x + other.x,
            .y = v.y + other.y,
            .z = v.z + other.z,
        };
    }
};

pub inline fn vec3(x: f32, y: f32, z: f32) Vec3 {
    return Vec3{
        .x = x,
        .y = y,
        .z = z,
    };
}

pub inline fn vec2(x: f32, y: f32) Vec2 {
    return Vec2{
        .x = x,
        .y = y,
    };
}

pub const Vec2 = struct {
    x: f32,
    y: f32,

    pub inline fn normalize(v: Vec2) Vec2 {
        return v.scale(1.0 / @sqrt(v.dot(v)));
    }

    pub inline fn scale(v: Vec2, scalar: f32) Vec2 {
        return Vec2{
            .x = v.x * scalar,
            .y = v.y * scalar,
        };
    }

    pub inline fn dot(v: Vec2, other: Vec2) f32 {
        return v.x * other.x + v.y * other.y;
    }

    pub inline fn length(v: Vec2) f32 {
        return @sqrt(v.dot(v));
    }

    /// returns the cross product
    pub inline fn cross(v: Vec2, other: Vec2) Vec2 {
        return Vec2{
            .x = v.y * other.z - other.y * v.z,
            .y = v.z * other.x - other.z * v.x,
        };
    }

    pub inline fn add(v: Vec2, other: Vec2) Vec2 {
        return Vec2{
            .x = v.x + other.x,
            .y = v.y + other.y,
        };
    }
};

pub const Color = struct {
    r: f32, g: f32, b: f32, a: f32
};

pub inline fn color(r: f32, g: f32, b: f32, a: f32) Color {
    return Color{
        .r = r,
        .g = g,
        .b = b,
        .a = a,
    };
}

pub const Point = struct {
    x: i32,
    y: i32,
};
