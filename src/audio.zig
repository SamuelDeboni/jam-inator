const c = @import("c.zig");
const std = @import("std");

pub const SoundBuffer = c.ALuint;
pub const SoundSpeaker = c.ALuint;

const AUDIO_GAIN_CONST: f32 = 0.1;
const AUDIO_PAN_CONST: f32 = 0.1;

fn gainFunction(x: f32, y: f32) f32 {
    return 1.0 / (x * x + y * y) * AUDIO_GAIN_CONST + 1.0;
}

fn panFunction(x: f32) f32 {
    const signal: f32 = if (x > 0.0) 1.0 else -1.0;
    const tmp: f32 = std.math.pow(f32, 1.0 + 1.0 / (x * AUDIO_PAN_CONST), x * AUDIO_PAN_CONST) - 1.0;
    return (signal * tmp) / 1.71828;
}

//-----------------------------------------------------------------------------

/// Play the song linked to speaker. If the speaker was previous paused the
/// execution will be started in the paused point. If was stoped the execution
/// will be started on begin.
pub fn playSpeaker(speaker: SoundSpeaker) void {
    c.alSourcePlay(speaker);
}

/// Pause speaker audio execution.
pub fn pauseSpeaker(speaker: SoundSpeaker) void {
    c.alSourcePause(speaker);
}

/// Stop speaker audio execution.
pub fn stopSpeaker(speaker: SoundSpeaker) void {
    c.alSourceStop(speaker);
}

/// Play the song linked to speaker from begin.
pub fn rewindSpeaker(speaker: SoundSpeaker) void {
    c.alSourceRewind(speaker);
}

//-----------------------------------------------------------------------------

/// Check OpenAL error, and if an error has detected, print OpenAL error code in
/// STDERR with context details.
/// params:
///     details - details about context of error verification.
pub fn checkAlError(details: []const u8) void {
    //check OpenAL error status
    var err: c.ALenum = c.alGetError();
    if (err != c.AL_NO_ERROR) {
        std.debug.warn("OpenAl error: {} on {}\n", .{ err, details });
        err = c.alGetError();
    }
}

/// Init audio library.
pub fn initAudioLib() !void {
    //open default device
    var device: ?*c.ALCdevice = c.alcOpenDevice(null);
    if (device) |dev| {
        //creating the current context
        var context: ?*c.ALCcontext = c.alcCreateContext(dev, null);
        if (c.alcMakeContextCurrent(context) == 0) {
            return error.ALContextCreationError;
        }
        checkAlError("context creation");

        //defing the listener
        c.alListener3f(c.AL_POSITION, 0.0, 0.0, 0.0);
        checkAlError("listener creation");
    } else {
        return error.ALDeviceCreationError;
    }
}

/// Change OpenAL current audio device.
/// params:
///     device_name - the name of new current device.
pub fn ifr_changeAudioDevice(device_name: []const u8) !void {
    //open default device
    var device: ?*c.ALCdevice = c.alcOpenDevice(device_name);
    if (device) |dev| {
        //creating the current context
        var context: ?*c.ALCcontext = c.alcCreateContext(dev, null);
        if (c.alcMakeContextCurrent(context) == 0) {
            return error.ALContextCreationError;
        }
        checkAlError("context creation");

        //defing the listener
        c.alListener3f(c.AL_POSITION, 0.0, 0.0, 0.0);
        checkAlError("listener creation");
    } else {
        return error.ALDeviceCreationError;
    }
}

/// Load a OGG audio file, and pass the generated sound buffer.
/// params:
///     names - the paths of audio files.
pub fn loadOGGAudioBufferFromData(data: []const u8) SoundBuffer {
    //creating a song buffer
    var buffer: SoundBuffer = undefined;
    c.alGenBuffers(1, &buffer);
    checkAlError("creation a new song buffer");

    var size: c.ALsizei = undefined;
    var freq: c.ALsizei = undefined;
    var format: c.ALenum = undefined;
    var channels: c.ALenum = undefined;
    var decoded_data: [*c]u8 = undefined;
    size = c.stb_vorbis_decode_memory(
        @ptrCast([*c]const u8, data[0..]),
        @intCast(c_int, data.len),
        &channels,
        &freq,
        @ptrCast([*c][*c]c_short, &decoded_data),
    );
    if (channels == 1) {
        format = c.AL_FORMAT_MONO16;
        size = size * 2;
    } else {
        format = c.AL_FORMAT_STEREO16;
        size = size * 4;
    }
    c.alBufferData(buffer, format, decoded_data, size, freq);
    checkAlError("defining a new song buffer");

    std.heap.c_allocator.free(decoded_data[0..@intCast(usize, size)]);
    return buffer;
}

/// Create a new sound buffer from input data.
/// params:
///     data - the raw song data.
///     channels - song channel qnt.
///     size - size in bytes of song.
///     freq - song frequency
pub fn createAudioBufferFromData(data: []u8, channels: u8, size: usize, freq: usize) SoundBuffer {
    var format: ALenum;
    var buffer: SoundBuffer;
    alGenBuffers(1, &buffer);
    checkAlError("creation a new song buffer");

    if (channels == 1) {
        format = c.AL_FORMAT_MONO16;
    } else {
        format = c.AL_FORMAT_STEREO16;
    }
    alBufferData(buffer, format, data, size, freq);
    checkAlError("defining a new song buffer");
    return buffer;
}

/// Delete a array of sound buffers.
/// params:
///     init - initial index in array for deletion.
///     qnt - quantity of elements for delete.
///     buffers - the buffers for delete.
pub fn deleteAudioBuffers(buffers: []SoundBuffer) void {
    for (buffers) |*it| {
        c.alDeleteBuffers(1, it);
        checkAlError("deleting audio buffers");
    }
}

/// Init a ifr_SoundSpeaker.
/// params:
///     speaker - the speaker for initialize.
///     loop - if `TRUE`, the speaker will loop the song when the song ends.
pub fn initSpeaker(speaker: *SoundSpeaker, loop: bool) void {
    //init the speaker
    c.alGenSources(1, speaker);
    c.alSourcei(speaker.*, c.AL_LOOPING, @boolToInt(loop));
    c.alSourcef(speaker.*, c.AL_ROLLOFF_FACTOR, 0.0);
    c.alSourcei(speaker.*, c.AL_SOURCE_RELATIVE, c.AL_TRUE);
    checkAlError("source creation");
}

/// Delete a SoundSpeaker.
/// params:
///     speaker - speaker for delete.
pub fn deleteSpeaker(speaker: *SoundSpeaker) void {
    c.alDeleteSources(1, speaker);
    checkAlError("deleting source");
}

/// Update a SoundSpeaker 2d position.
/// params:
///     speaker - speaker for update.
///     pos - the vector of new 2d speaker position.
///     volum - a gain modificator.
pub fn updateSpeaker(speaker: SoundSpeaker, x: f32, y: f32, volum: f32) void {
    //calculate pan
    c.alSource3f(speaker, c.AL_POSITION, panFunction(x), panFunction(y), 0.1);

    //calculate gain
    c.alSourcef(speaker, c.AL_GAIN, gainFunction(x, y) * volum);
}

/// Bind a ifr_SoundBuffer to ifr_SoundSpeaker.
pub fn loadSongToSpeaker(speaker: SoundSpeaker, buffer: SoundBuffer) void {
    //bind song into speaker
    c.alSourcei(speaker, c.AL_BUFFER, @intCast(c_int, buffer));
    checkAlError("binding song to source");
}
