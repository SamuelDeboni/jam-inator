const std = @import("std");
const c = @import("c.zig");
const r2d = @import("render2d.zig");

const assets = @import("assets.zig");
const Texture = assets.Texture;
const Image = assets.Image;

//pub const audio = @import("audio.zig");

usingnamespace @import("math3d.zig");

pub const input = struct {
    const keymap = [_]u32{
        c.GLFW_KEY_W,
        c.GLFW_KEY_A,
        c.GLFW_KEY_S,
        c.GLFW_KEY_D,

        c.GLFW_KEY_UP,
        c.GLFW_KEY_DOWN,
        c.GLFW_KEY_RIGHT,
        c.GLFW_KEY_LEFT,

        c.GLFW_KEY_SPACE,
        c.GLFW_KEY_LEFT_SHIFT,
        c.GLFW_KEY_LEFT_CONTROL,

        c.GLFW_KEY_E,
        c.GLFW_KEY_Q,
    };

    pub const keys = enum(u32) {
        w = (1 << 0),
        a = (1 << 1),
        s = (1 << 2),
        d = (1 << 3),

        up = (1 << 4),
        down = (1 << 5),
        left = (1 << 6),
        right = (1 << 7),

        space = (1 << 8),
        shift = (1 << 9),
        ctrl = (1 << 10),

        e = (1 << 11),
        q = (1 << 12),
    };

    pub var input_down: u32 = 0;
    pub var input_up: u32 = 0;
    pub var input_pressed: u32 = 0;

    pub fn callback(
        window: ?*c.GLFWwindow,
        key: c_int,
        scancode: c_int,
        action: c_int,
        mods: c_int,
    ) callconv(.C) void {
        switch (action) {
            c.GLFW_PRESS => {
                inline for (keymap) |it, i| {
                    const bit = @intCast(u32, @shlExact(1, i));
                    if (key == it) {
                        if ((input_pressed & bit) != bit) {
                            input_down = input_down | bit;
                        }
                        input_pressed = input_pressed | bit;
                    }
                }
            },
            c.GLFW_RELEASE => {
                inline for (keymap) |it, i| {
                    const bit = @intCast(u32, @shlExact(1, i));
                    if (key == it) {
                        if ((input_pressed & bit) == bit) {
                            input_up = input_up | bit;
                        }
                        input_pressed = input_pressed & (~bit);
                    }
                }
            },
            else => {},
        }
    }

    pub var mouse_posx_raw: f64 = 0.0;
    pub var mouse_posy_raw: f64 = 0.0;
    pub fn cursorPosCallback(
        window: ?*c.GLFWwindow,
        xpos: f64,
        ypos: f64,
    ) callconv(.C) void {
        mouse_posx_raw = xpos;
        mouse_posy_raw = ypos;
    }

    pub const mouse_buttons = enum(u8) {
        left = (1 << 0),
        right = (1 << 1),
        middle = (1 << 2),
    };

    pub var mouse_pressed: u8 = 0;
    pub var mouse_up: u8 = 0;
    pub var mouse_down: u8 = 0;

    pub fn mouseButtonCallback(
        window: ?*c.GLFWwindow,
        button: c_int,
        action: c_int,
        mods: c_int,
    ) callconv(.C) void {
        switch (action) {
            c.GLFW_PRESS => {
                var i: u3 = 0;
                while (i < 4) : (i += 1) {
                    const bit = @intCast(u8, @as(u8, 1) << i);
                    if (button == i) {
                        if ((mouse_pressed & bit) != bit) {
                            mouse_down = mouse_down | bit;
                        }
                        mouse_pressed = mouse_pressed | bit;
                    }
                }
            },
            c.GLFW_RELEASE => {
                var i: u3 = 0;
                while (i < 4) : (i += 1) {
                    const bit = @intCast(u8, @as(u8, 1) << i);
                    if (button == i) {
                        if ((mouse_pressed & bit) == bit) {
                            mouse_up = mouse_up | bit;
                        }
                        mouse_pressed = mouse_pressed & (~bit);
                    }
                }
            },
            else => {},
        }
    }

    pub fn init(window: *c.GLFWwindow) void {
        _ = c.glfwSetKeyCallback(window, callback);
        _ = c.glfwSetCursorPosCallback(window, cursorPosCallback);
        _ = c.glfwSetMouseButtonCallback(window, mouseButtonCallback);
    }

    pub inline fn resetDownAndUpInputs() void {
        input_down = 0;
        input_up = 0;
        mouse_down = 0;
        mouse_up = 0;
    }

    pub inline fn keyDown(key: keys) bool {
        return (@enumToInt(key) & input_down) == @enumToInt(key);
    }

    pub inline fn keyUp(key: keys) bool {
        return (@enumToInt(key) & input_up) == @enumToInt(key);
    }

    pub inline fn keyPressed(key: keys) bool {
        return (@enumToInt(key) & input_pressed) == @enumToInt(key);
    }

    pub inline fn mouseDown(button: mouse_buttons) bool {
        return (@enumToInt(button) & mouse_down) == @enumToInt(button);
    }

    pub inline fn mouseUp(button: mouse_buttons) bool {
        return (@enumToInt(button) & mouse_up) == @enumToInt(button);
    }

    pub inline fn mousePressed(button: mouse_buttons) bool {
        return (@enumToInt(button) & mouse_pressed) == @enumToInt(button);
    }
};

/// This is the Global context struct
/// all global variables go inside this struct and it
/// is passed to any functions that need-it
pub const Ctx = struct {
    window: *c.GLFWwindow,
    window_width: u32,
    window_height: u32,
};

var main_ctx: Ctx = undefined;

fn errorCallback(err: c_int, msg: [*c]const u8) callconv(.C) void {
    std.debug.panic("\n GLFW error callback n:{}\n{}\n", .{ err, msg });
}

fn framebufferSizeCallback(
    window: ?*c.GLFWwindow,
    width: c_int,
    height: c_int,
) callconv(.C) void {
    c.glViewport(
        0,
        0,
        @intCast(c_int, width),
        @intCast(c_int, height),
    );
    main_ctx.window_height = @intCast(u32, height);
    main_ctx.window_width = @intCast(u32, width);
}

/// Init GLFW and OpenGL and return a context
pub fn init(
    window_width: u32,
    window_height: u32,
    title_name: [*:0]const u8,
    start_callback: fn (ctx: Ctx) void,
    update_callback: fn (ctx: Ctx, delta: f64) void,
) !Ctx {
    try assets.init();

    _ = c.glfwSetErrorCallback(errorCallback);
    if (c.glfwInit() == c.GL_FALSE) {
        std.debug.panic("GLFW init failure\n", .{});
    }

    c.glfwWindowHint(c.GLFW_CONTEXT_VERSION_MAJOR, 3);
    c.glfwWindowHint(c.GLFW_CONTEXT_VERSION_MINOR, 2);
    c.glfwWindowHint(c.GLFW_OPENGL_FORWARD_COMPAT, c.GL_TRUE);
    c.glfwWindowHint(c.GLFW_OPENGL_PROFILE, c.GLFW_OPENGL_CORE_PROFILE);
    c.glfwWindowHint(c.GLFW_RESIZABLE, c.GL_FALSE);
    c.glfwWindowHint(c.GLFW_SAMPLES, 4);

    // Create window
    const window: *c.GLFWwindow = c.glfwCreateWindow(
        @intCast(c_int, window_width),
        @intCast(c_int, window_height),
        title_name,
        null,
        null,
    ) orelse {
        c.glfwTerminate();
        std.debug.panic("unable to create window\n", .{});
    };

    // Create open gl context
    c.glfwMakeContextCurrent(window);

    if (c.gladLoadGLLoader(@ptrCast(c.GLADloadproc, c.glfwGetProcAddress)) == 0) {
        std.debug.panic("unable to initialize glad", .{});
    }

    c.glClearColor(0.0, 0.0, 0.0, 1.0);

    // Enable depth test
    c.glEnable(c.GL_DEPTH_TEST);
    c.glDepthFunc(c.GL_LEQUAL);

    // Enable transparency
    c.glEnable(c.GL_BLEND);
    c.glBlendFunc(c.GL_SRC_ALPHA, c.GL_ONE_MINUS_SRC_ALPHA);

    // Enable face culling
    c.glEnable(c.GL_CULL_FACE);
    c.glCullFace(c.GL_BACK);
    c.glFrontFace(c.GL_CCW);

    // Antialiasing
    c.glEnable(c.GL_MULTISAMPLE);

    _ = c.glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    // Initialize main context
    main_ctx = Ctx{
        .window = window,
        .window_width = window_width,
        .window_height = window_height,
    };
    c.glfwSetInputMode(window, c.GLFW_STICKY_KEYS, c.GLFW_FALSE);

    r2d.init();

    // initialize input
    input.init(window);

    // init audio
    //try audio.initAudioLib();

    start_callback(main_ctx);

    var delta: f64 = 0.0; // Delta time between frames
    var itime: f64 = c.glfwGetTime(); // Start time of the frame
    var ltime: f64 = 0.0;

    while (c.glfwWindowShouldClose(main_ctx.window) == 0) {
        ltime = itime;
        itime = c.glfwGetTime(); // Get the initial frame time
        delta = itime - ltime;
        if (delta > 0.1) // Prevent delta to go to large in case of lag
            delta = 0.1;

        c.glClear(c.GL_COLOR_BUFFER_BIT | c.GL_DEPTH_BUFFER_BIT);
        update_callback(main_ctx, delta);
        c.glfwSwapBuffers(main_ctx.window);

        input.resetDownAndUpInputs();
        c.glfwPollEvents();
    }
    return main_ctx;
}

/// It is called at the end, and it does clenaup
pub fn quit(ctx: Ctx) void {
    c.glfwDestroyWindow(ctx.window);
    c.glfwTerminate();
}

/// Assert tha there is no OpgenGl errors
pub fn assertGl() void {
    const err: c.GLenum = c.glGetError();
    if (err != c.GL_NO_ERROR)
        std.debug.panic("\nOpenGL Error {}\n", .{err});
}
