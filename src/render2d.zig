/// This file has sprites based rendering
/// functions for OpenGL
const std = @import("std");
const c = @import("c.zig");

const assertGl = @import("core.zig").assertGl;

const assets = @import("assets.zig");
const Texture = assets.Texture;

usingnamespace @import("math3d.zig");

// === Shader stuff ==========

pub const shaders = struct {
    /// Typedef of a OpenGL Shader
    pub const Shader = c.GLuint;

    /// Use a Shader program
    pub inline fn use(shader: Shader) void {
        c.glUseProgram(shader);
    }

    /// Set the uniform1i of a shader, note: remember to activate the shader before
    pub inline fn setUniform1i(
        shader: Shader,
        name: [*:0]const u8,
        value: i32,
    ) void {
        const location = c.glGetUniformLocation(shader, name);
        c.glUniform1i(location, value);
    }

    /// Set the uniform4f of a shader, note: remember to activate the shader before
    pub inline fn setUniform4f(
        shader: Shader,
        name: [*:0]const u8,
        x: f32,
        y: f32,
        z: f32,
        w: f32,
    ) void {
        const location = c.glGetUniformLocation(shader, name);
        c.glUniform4f(location, x, y, z, w);
    }

    /// Set the uniform4fv of a shader, note: remember to activate the shader before
    pub inline fn setUniformMatrix4fv(
        shader: Shader,
        name: [*:0]const u8,
        value: [4][4]f32,
    ) void {
        const location = c.glGetUniformLocation(shader, name);
        c.glUniformMatrix4fv(location, 1, c.GL_FALSE, @ptrCast([*c]const f32, &value));
    }

    /// Compile a OpenGL shader given a source and type
    pub fn compileShader(
        source: [*:0]const u8,
        shader_type: c.GLenum,
    ) Shader {
        const shader: Shader = c.glCreateShader(shader_type);
        c.glShaderSource(shader, 1, &source, null);
        c.glCompileShader(shader);

        var status: c_int = 0;
        c.glGetShaderiv(shader, c.GL_COMPILE_STATUS, &status);
        if (status == 0) {
            var buffer: [256:0]u8 = undefined;
            c.glGetShaderInfoLog(shader, 256, null, &buffer);
            std.debug.panic(
                \\
                \\---- Shader Error ----
                \\Shader {} failed to compile
                \\Status {}
                \\{}-------- End ---------
                \\
            , .{ shader, status, buffer });
        } else {
            //std.debug.warn("\nShader {} compiled\n", .{shader});
        }

        return shader;
    }

    /// Compile and create a OpenGL shader program
    pub fn createShaderProgram(
        vertex_source: [*:0]const u8,
        fragment_source: [*:0]const u8,
    ) Shader {
        const vertexShader = compileShader(vertex_source, c.GL_VERTEX_SHADER);
        defer c.glDeleteShader(vertexShader);
        const fragmentShader = compileShader(fragment_source, c.GL_FRAGMENT_SHADER);
        defer c.glDeleteShader(fragmentShader);

        const shader_program = c.glCreateProgram();
        c.glAttachShader(shader_program, vertexShader);
        c.glAttachShader(shader_program, fragmentShader);

        c.glBindFragDataLocation(shader_program, 0, "outColor");
        c.glLinkProgram(shader_program);

        assertGl();

        return shader_program;
    }
};

// === Structs declaration ===

/// Viewport used to render sprites
/// width height and offsets are in pixels
pub const Viewport = struct {
    width: f32,
    height: f32,
    x_offset: f32,
    y_offset: f32,
    pixel_per_unity: f32,
};

/// A simple rect
/// x, y -> top-left positions
/// w, h -> width and height
pub const Rect = struct {
    x: f32,
    y: f32,
    w: f32,
    h: f32,
};

/// A simple constructor function
pub fn rectc(
    x: f32,
    y: f32,
    w: f32,
    h: f32,
) Rect {
    return Rect{
        .x = x,
        .y = y,
        .w = w,
        .h = h,
    };
}

pub inline fn drawBitmapStringFmt(
    comptime fmt: []const u8,
    args: var,
    len: u8,
    bitmap: Texture,
    rect: Rect,
    font_width: u32,
    font_height: u32,
    z_pos: f32,
    viewport: Viewport,
) void {
    var buf: [256]u8 = undefined;
    for (buf) |*it| it.* = ' ';
    var s = std.fmt.bufPrint(&buf, fmt, args);

    drawBitmapString(
        buf[0..],
        len,
        bitmap,
        rect,
        font_width,
        font_height,
        z_pos,
        viewport,
    );
}

pub inline fn drawBitmapString(
    str: []const u8,
    len: u8,
    bitmap: Texture,
    rect: Rect,
    font_width: u32,
    font_height: u32,
    z_pos: f32,
    viewport: Viewport,
) void {
    c.glBindVertexArray(sprite_vao);
    shaders.use(sprite_shader);

    const view_w = viewport.width / viewport.pixel_per_unity;
    const view_h = viewport.height / viewport.pixel_per_unity;
    const view_x = viewport.x_offset;
    const view_y = viewport.y_offset;

    const xt = 2.0 / view_w;
    const yt = 2.0 / view_h;

    const w = (rect.w) * xt;
    const h = (rect.h) * yt;

    var pos_y = (rect.y - view_y) * yt;

    const fw = @intToFloat(f32, font_width) / @intToFloat(f32, bitmap.width);
    const fh = @intToFloat(f32, font_height) / @intToFloat(f32, bitmap.height);

    var x_pos: usize = 0;
    for (str) |it, i| {
        if (i >= len) break;
        x_pos += 1;

        if (it == '\n') {
            x_pos = 0;
            pos_y -= rect.h * 0.5;
            continue;
        }

        const pos_x = (rect.x - view_x + @intToFloat(f32, x_pos) * rect.w) * xt;

        c.glActiveTexture(c.GL_TEXTURE0);
        c.glBindTexture(c.GL_TEXTURE_2D, bitmap.id);
        shaders.setUniform1i(sprite_shader, "tex", 0);

        const fx = @intToFloat(f32, (it) % 16) * fw;
        const fy = @intToFloat(f32, (it) / 16) * fh;

        shaders.setUniform4f(sprite_shader, "crop", fx, fy, fw, fh);

        shaders.setUniform4f(
            sprite_shader,
            "color",
            bitmap.color.r,
            bitmap.color.g,
            bitmap.color.b,
            bitmap.color.a,
        );
        const view_mat = Mat4_identity.translate(pos_x, pos_y, z_pos).scale(w, h, 1);

        shaders.setUniformMatrix4fv(sprite_shader, "viewMatrix", view_mat.data);

        c.glDrawElements(c.GL_TRIANGLES, 6, c.GL_UNSIGNED_BYTE, null);
        assertGl();
    }
    c.glBindVertexArray(0);
}

pub inline fn drawBitmapStringFmtPx(
    comptime fmt: []const u8,
    args: var,
    len: u8,
    bitmap: Texture,
    rect: Rect,
    font_width: u32,
    font_height: u32,
    z_pos: f32,
    viewport: Viewport,
) void {
    var buf: [256]u8 = undefined;
    for (buf) |*it| it.* = ' ';
    var s = std.fmt.bufPrint(&buf, fmt, args);

    drawBitmapStringPx(
        buf[0..],
        len,
        bitmap,
        rect,
        font_width,
        font_height,
        z_pos,
        viewport,
    );
}

pub inline fn drawBitmapStringPx(
    str: []u8,
    len: u8,
    bitmap: Texture,
    rect: Rect,
    font_width: u32,
    font_height: u32,
    z_pos: f32,
    viewport: Viewport,
) void {
    c.glBindVertexArray(sprite_vao);
    shaders.use(sprite_shader);

    const view_w = viewport.width / viewport.pixel_per_unity;
    const view_h = viewport.height / viewport.pixel_per_unity;
    const view_x = viewport.x_offset;
    const view_y = viewport.y_offset;

    const xt = 2.0 / view_w;
    const yt = 2.0 / view_h;

    const rectx = rect.x / viewport.pixel_per_unity;
    const recty = rect.y / viewport.pixel_per_unity;
    const rectw = (rect.w) / viewport.pixel_per_unity;
    const recth = rect.h / viewport.pixel_per_unity;

    const w = (rectw) * xt;
    const h = (recth) * yt;

    var pos_y = (recty - view_y) * yt;

    const fw = @intToFloat(f32, font_width) / @intToFloat(f32, bitmap.width);
    const fh = @intToFloat(f32, font_height) / @intToFloat(f32, bitmap.height);

    var x_pos: usize = 0;
    for (str) |it, i| {
        if (i >= len) break;
        x_pos += 1;

        if (it == '\n') {
            x_pos = 0;
            pos_y -= recth * 0.5;
            continue;
        }

        const pos_x = (rectx - view_x + @intToFloat(f32, x_pos) * rectw) * xt;

        c.glActiveTexture(c.GL_TEXTURE0);
        c.glBindTexture(c.GL_TEXTURE_2D, bitmap.id);
        shaders.setUniform1i(sprite_shader, "tex", 0);

        const fx = @intToFloat(f32, (it) % 16) * fw;
        const fy = @intToFloat(f32, (it) / 16) * fh;

        shaders.setUniform4f(sprite_shader, "crop", fx, fy, fw, fh);

        shaders.setUniform4f(
            sprite_shader,
            "color",
            bitmap.color.r,
            bitmap.color.g,
            bitmap.color.b,
            bitmap.color.a,
        );
        const view_mat = Mat4_identity.translate(pos_x, pos_y, z_pos).scale(w, h, 1);

        shaders.setUniformMatrix4fv(sprite_shader, "viewMatrix", view_mat.data);

        c.glDrawElements(c.GL_TRIANGLES, 6, c.GL_UNSIGNED_BYTE, null);
        assertGl();
    }
    c.glBindVertexArray(0);
}

pub var sprite_shader: u32 = undefined;
pub var sprite_vao: u32 = undefined;

/// Creates the default shader and vao for sprite render
/// Must be done at runtime because it is bind to the GL context
pub fn init() void {
    sprite_shader = shaders.createShaderProgram(
        \\#version 330 core
        \\layout (location = 0) in vec2 position; 
        \\out vec2 uvCord;
        \\
        \\uniform vec4 crop;
        \\uniform mat4 viewMatrix;
        \\
        \\void main() {
        \\  vec2 cord = vec2(crop.x, crop.y);
        \\  if (gl_VertexID > 1)
        \\      cord.x = crop.z + crop.x;
        \\  if (gl_VertexID == 1 || gl_VertexID == 2)
        \\      cord.y = crop.y + crop.w;
        \\  uvCord = cord;
        \\  gl_Position = vec4(position, 0.0, 1.0) * viewMatrix;  
        \\}
    ,
        \\#version 330 core
        \\in vec2 uvCord;
        \\out vec4 outColor;
        \\
        \\uniform sampler2D tex;
        \\uniform vec4 color;
        \\
        \\void main() {
        \\  outColor = texture(tex, uvCord) * color; 
        \\}
    );
    sprite_vao = createVao: {
        const vertex = [_]f32{
            -0.5, 0.5,
            -0.5, -0.5,
            0.5,  -0.5,
            0.5,  0.5,
        };
        const elements = [_]u8{
            0, 1, 2, 0, 2, 3,
        };

        var vao: c.GLuint = 0;
        c.glGenVertexArrays(1, &vao);
        c.glBindVertexArray(vao);

        var vb: c.GLuint = 0;
        c.glGenBuffers(1, &vb);
        c.glBindBuffer(c.GL_ARRAY_BUFFER, vb);
        c.glBufferData(c.GL_ARRAY_BUFFER, 8 * @sizeOf(f32), &vertex[0], c.GL_STATIC_DRAW);

        var eb: c.GLuint = 0;
        c.glGenBuffers(1, &eb);
        c.glBindBuffer(c.GL_ELEMENT_ARRAY_BUFFER, eb);
        c.glBufferData(c.GL_ELEMENT_ARRAY_BUFFER, 6 * @sizeOf(u8), &elements[0], c.GL_STATIC_DRAW);

        shaders.use(sprite_shader);
        const pos_attr: u32 = 0;
        c.glEnableVertexAttribArray(pos_attr);
        c.glVertexAttribPointer(pos_attr, 2, c.GL_FLOAT, c.GL_FALSE, 2 * @sizeOf(f32), null);

        c.glBindVertexArray(0);
        break :createVao vao;
    };
    assertGl();
}

/// Render a texture based in a given rect
pub fn drawTexture(
    texture: Texture,
    rect: Rect,
    crop: Rect,
    viewport: Viewport,
    z_pos: f32,
    angle: f32,
) void {
    const view_w = viewport.width / viewport.pixel_per_unity;
    const view_h = viewport.height / viewport.pixel_per_unity;

    const view_x = viewport.x_offset;
    const view_y = viewport.y_offset;

    const xt = 2.0 / view_w;
    const yt = 2.0 / view_h;
    const pos_x = (rect.x - view_x) * xt;
    const pos_y = (rect.y - view_y) * yt;

    const w = (rect.w) * xt;
    const h = (rect.h) * yt;

    c.glBindVertexArray(sprite_vao);
    shaders.use(sprite_shader);

    c.glActiveTexture(c.GL_TEXTURE0);
    c.glBindTexture(c.GL_TEXTURE_2D, texture.id);
    shaders.setUniform1i(sprite_shader, "tex", 0);

    shaders.setUniform4f(sprite_shader, "crop", crop.x, crop.y, crop.w, crop.h);

    shaders.setUniform4f(
        sprite_shader,
        "color",
        texture.color.r,
        texture.color.g,
        texture.color.b,
        texture.color.a,
    );
    const view_mat = if (angle == 0.0)
        Mat4_identity.translate(pos_x, pos_y, z_pos).scale(w, h, 1)
    else
        Mat4_identity.translate(pos_x, pos_y, z_pos).scale(w, h, 1).rotate(angle, .{ .x = 0, .y = 0, .z = 1 });

    shaders.setUniformMatrix4fv(sprite_shader, "viewMatrix", view_mat.data);

    c.glDrawElements(c.GL_TRIANGLES, 6, c.GL_UNSIGNED_BYTE, null);
    assertGl();

    c.glBindVertexArray(0);
}

/// Render a texture based in a given rect
/// Similar to drawTexture, but the rect
/// is in pixels
pub fn drawTexturePx(
    texture: Texture,
    rect: Rect,
    crop: Rect,
    viewport: Viewport,
    z_pos: f32,
    angle: f32,
) void {
    const view_w = viewport.width / viewport.pixel_per_unity;
    const view_h = viewport.height / viewport.pixel_per_unity;

    const view_x = viewport.x_offset;
    const view_y = viewport.y_offset;

    const rectx = rect.x / viewport.pixel_per_unity;
    const recty = rect.y / viewport.pixel_per_unity;
    const rectw = rect.w / viewport.pixel_per_unity;
    const recth = rect.h / viewport.pixel_per_unity;

    const xt = 2.0 / view_w;
    const yt = 2.0 / view_h;
    const pos_x = (rectx - view_x) * xt;
    const pos_y = (recty - view_y) * yt;

    const w = (rectw) * xt;
    const h = (recth) * yt;

    c.glBindVertexArray(sprite_vao);
    shaders.use(sprite_shader);

    c.glActiveTexture(c.GL_TEXTURE0);
    c.glBindTexture(c.GL_TEXTURE_2D, texture.id);
    shaders.setUniform1i(sprite_shader, "tex", 0);

    shaders.setUniform4f(
        sprite_shader,
        "crop",
        crop.x / @intToFloat(f32, texture.width),
        crop.y / @intToFloat(f32, texture.height),
        crop.w / @intToFloat(f32, texture.width),
        crop.h / @intToFloat(f32, texture.height),
    );

    shaders.setUniform4f(
        sprite_shader,
        "color",
        texture.color.r,
        texture.color.g,
        texture.color.b,
        texture.color.a,
    );
    const view_mat = if (angle == 0.0)
        Mat4_identity.translate(pos_x, pos_y, z_pos).scale(w, h, 1)
    else
        Mat4_identity.translate(pos_x, pos_y, z_pos).scale(w, h, 1).rotate(angle, .{ .x = 0, .y = 0, .z = 1 });

    shaders.setUniformMatrix4fv(sprite_shader, "viewMatrix", view_mat.data);

    c.glDrawElements(c.GL_TRIANGLES, 6, c.GL_UNSIGNED_BYTE, null);
    assertGl();

    c.glBindVertexArray(0);
}
