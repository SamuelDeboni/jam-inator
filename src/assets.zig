const std = @import("std");
const c = @import("c.zig");
const assertGl = @import("core.zig").assertGl;
usingnamespace @import("math3d.zig");

// this are fallback images
pub var potato_img: Image = undefined;
pub var font_img: Image = undefined;

/// Loads fallback textures
pub fn init() !void {
    // Load fallback textures
    potato_img = try Image.createFromBmpData(@embedFile("../assets/potato.bmp"));
    font_img = try Image.createFromBmpData(@embedFile("../assets/font.bmp"));
}

const embed_bmps = struct {
    pub const slime = @embedFile("../assets/Slime.bmp");
    pub const spiky_slime = @embedFile("../assets/SpikySlime.bmp");

    pub const healer = @embedFile("../assets/Healer.bmp");
    pub const healer_hurt = @embedFile("../assets/HealerHurt.bmp");

    pub const knight = @embedFile("../assets/Knight.bmp");
    pub const knight_hurt = @embedFile("../assets/KnightHurt.bmp");

    pub const gost_tree = @embedFile("../assets/GostTree.bmp");
    pub const geon = @embedFile("../assets/Geon.bmp");
};

/// Given a comptime string it will return a glTexture from
/// the field in the embed_bmp struct with the name given by
/// the string
///
/// If the asset does not exist it will use a fallback image
pub fn embedBmpToGlTexture(comptime name: []const u8) Texture {
    const image = Image.createFromBmpData(
        @field(embed_bmps, name)[0..],
    ) catch potato_img;

    const result = image.toGlTexture();
    return result;
}

const BmpHeader = packed struct {
    file_type: u16,
    file_size: u32,
    reserved1: u16,
    reserved2: u16,
    bitmap_offset: u32,

    size: u32,
    width: i32,
    height: i32,
    planes: u16,
    bits_per_pixel: u16,

    compression: u32,
    size_of_bitmap: u32,
    horz_resolution: i32,
    vert_resolution: i32,
    colors_used: u32,
    colors_important: u32,
};

pub const Image = struct {
    width: u32,
    height: u32,
    pitch: u32,
    raw: []const u8,
    gl_mode: c_uint,

    pub fn destroy(im: Image) void {
        c.stbi_image_free(im);
    }

    /// This function is problably very unsafe
    /// it will change later
    pub fn createFromBmpData(bmp_data: []const u8) !Image {
        const header: *const BmpHeader = @ptrCast(
            *const BmpHeader,
            bmp_data[0..@sizeOf(BmpHeader)],
        );

        if (header.file_type != 0x4d42) return error.NotBmpFile;
        if (header.compression != 3) return error.CompressedFile;
        if (header.bits_per_pixel != 32) return error.InvalidBitsPerPixel;

        var result_image: Image = undefined;
        result_image.width = @intCast(u32, header.width);
        result_image.height = @intCast(u32, header.height);
        result_image.pitch = 0;
        result_image.raw = bmp_data[header.bitmap_offset..];
        result_image.gl_mode = c.GL_BGRA;

        return result_image;
    }

    pub fn createFromPngData(png_data: []const u8) !Image {
        var im: Image = undefined;

        var width: c_int = undefined;
        var height: c_int = undefined;

        if (c.stbi_info_from_memory(
            png_data.ptr,
            @intCast(c_int, png_data.len),
            &width,
            &height,
            null,
        ) == 0) {
            return error.NotPngFile;
        }

        if (width <= 0 or height <= 0) return error.NoPixels;
        im.width = @intCast(u32, width);
        im.height = @intCast(u32, height);

        if (c.stbi_is_16_bit_from_memory(
            png_data.ptr,
            @intCast(c_int, png_data.len),
        ) != 0) {
            return error.InvalidFormat;
        }
        const bits_per_channel = 8;
        const channel_count = 4;

        const image_data = c.stbi_load_from_memory(
            png_data.ptr,
            @intCast(c_int, png_data.len),
            &width,
            &height,
            null,
            channel_count,
        );

        if (image_data == null) return error.NoMem;

        im.pitch = im.width * bits_per_channel * channel_count / 8;
        im.raw = image_data[0 .. im.height * im.pitch];

        return im;
    }

    pub fn toGlTexture(im: Image) Texture {
        var id: c.GLuint = 0;
        c.glGenTextures(1, &id);
        c.glBindTexture(c.GL_TEXTURE_2D, id);

        //const mode: c_int = c.GL_RGBA;
        c.glTexImage2D(
            c.GL_TEXTURE_2D,
            0,
            c.GL_RGBA,
            @intCast(c_int, im.width),
            @intCast(c_int, im.height),
            0,
            im.gl_mode,
            c.GL_UNSIGNED_BYTE,
            im.raw.ptr,
        );

        // Texture filtering
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MIN_FILTER, c.GL_NEAREST);
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MAG_FILTER, c.GL_NEAREST);
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_WRAP_S, c.GL_CLAMP_TO_EDGE);
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_WRAP_T, c.GL_CLAMP_TO_EDGE);
        assertGl();

        const tex = Texture{
            .id = id,
            .width = @intCast(i32, im.width),
            .height = @intCast(i32, im.height),
            .color = color(1.0, 1.0, 1.0, 1.0),
        };
        return tex;
    }
};

/// OpenGL texture
pub const Texture = struct {
    id: c.GLuint,
    width: i32,
    height: i32,
    color: Color,
};
