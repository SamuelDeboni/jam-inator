pub usingnamespace @cImport({
    @cInclude("glad/glad.h");
    @cInclude("GLFW/glfw3.h");

    @cDefine("STBI_ONLY_PNG", "");
    @cDefine("STBI_NO_STDIO", "");
    @cInclude("stb_image.h");

    @cInclude("AL/al.h");
    @cInclude("AL/alc.h");

    @cInclude("lua.h");
    @cInclude("lualib.h");
    @cInclude("lauxlib.h");

    @cInclude("stb_vorbis_header.h");
});
