const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    var target = b.standardTargetOptions(.{});
    target.abi = .musl;

    const mode = b.standardReleaseOptions();
    const windows = b.option(bool, "windows", "create windows build") orelse false;

    var exe = b.addExecutable("game", "src/main.zig");
    exe.addCSourceFile("stb_image/stb_image_impl.c", &[_][]const u8{"-std=c99"});
    exe.addCSourceFile("include/stb_vorbis.c", &[_][]const u8{"-std=c99"});
    exe.addCSourceFile("include/glad/glad.c", &[_][]const u8{"-std=c99"});
    exe.setTarget(target);

    if (windows) {
        exe.setTarget(.{
            .cpu_arch = .x86_64,
            .os_tag = .windows,
            .abi = .gnu,
        });
    }

    exe.setBuildMode(mode);
    exe.addIncludeDir("stb_image");
    exe.addIncludeDir("ifr_sound");
    exe.addIncludeDir("include");

    exe.linkSystemLibrary("c");
    if (windows) {
        exe.linkSystemLibrary("gdi32");
        exe.linkSystemLibrary("shell32");
        exe.addLibPath("lib");
    }
    exe.linkSystemLibrary("glfw3");
    //exe.linkSystemLibrary("openal");
    exe.linkSystemLibrary("lua");
    exe.linkSystemLibrary("m");
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
